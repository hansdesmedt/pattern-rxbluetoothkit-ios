//
//  Discover.swift
//  RxBluetoothKitSampleApp
//
//  Created by Hans De Smedt on 07/12/2018.
//  Copyright © 2018 In The Pocket. All rights reserved.
//

import RxSwift
import RxBluetoothKit

struct Discoverable {
    var readCharacteristic: Characteristic
    var writeCharacteristic: Characteristic
    var peripheral: Peripheral
}

final class Discover {
    
    private var SERVICE_UUID = "2141E110-213A-11E6-B67B-9E71128CAE77".uppercased()
    private var READ_CHARACTERISTIC_UUID = "2141e111-213a-11e6-b67b-9e71128cae77".uppercased()
    private var WRITE_CHARACTERISTIC_UUID = "2141e112-213a-11e6-b67b-9e71128cae77".uppercased()
    
    var disposeBag = DisposeBag()
    let loggingSubject = PublishSubject<String>()
    
    private let bluetoothService: RxBluetoothKitService
    private var peripheral: Peripheral? = nil
    private let scanSubject = PublishSubject<Discoverable>()

    
    init(bluetoothService: RxBluetoothKitService) {
        self.bluetoothService = bluetoothService
    }
    
    private func subscribe(deviceName: String) {
        bluetoothService.scanningOutput.subscribe(onNext: { [unowned self] (result) in
            switch result {
            case .success(let value):
                if let localName = value.advertisementData.localName {
                    self.loggingSubject.onNext("Scanning found \(localName)")
                    if localName == deviceName {
                        self.peripheral = value.peripheral
                        self.bluetoothService.discoverServices(for: value.peripheral)
                    }
                }
            case .error(let error):
                self.loggingSubject.onNext("Scanning error \(error)")
            }
        }, onError: { (error) in
            self.scanSubject.onError(error)
            self.loggingSubject.onNext("Scanning error \(error)")
        }, onCompleted: {
            self.loggingSubject.onNext("Scanning completed")
        }, onDisposed: nil).disposed(by: disposeBag)
        
        bluetoothService.discoveredServicesOutput.asObservable().subscribe(onNext: { [unowned self] (result) in
            switch result {
            case .success(let services):
                self.loggingSubject.onNext("Discover services success")
                services.forEach { service in
                    self.loggingSubject.onNext("Service discovered \(service.uuid)")
                    if service.uuid.uuidString == self.SERVICE_UUID {
                        self.bluetoothService.discoverCharacteristics(for: service)
                    }
                }
            case .error(let error):
                self.loggingSubject.onNext("Service discovered error \(error)")
            }
        }).disposed(by: disposeBag)
        
        bluetoothService.discoveredCharacteristicsOutput.subscribe(onNext: { [unowned self] result in
            switch result {
            case .success(let characteristics):
                characteristics.forEach({ (characteristic) in
                    self.loggingSubject.onNext("Characteristic discovered \(characteristic.uuid.uuidString)")
                })
                let readCharacteristic = characteristics.first(where: { $0.uuid.uuidString == self.READ_CHARACTERISTIC_UUID })
                let writeCharacteristic = characteristics.first(where: { $0.uuid.uuidString == self.WRITE_CHARACTERISTIC_UUID })
                if let read = readCharacteristic, let write = writeCharacteristic, let peripheral = self.peripheral {
                    self.scanSubject.onNext(Discoverable(readCharacteristic: read, writeCharacteristic: write, peripheral: peripheral))
                }
                self.bluetoothService.stopScanning()
            case .error(let error):
                self.loggingSubject.onNext("Characteristic discovered error \(error)")
            }
        }).disposed(by: disposeBag)
    }
    
    func scan(deviceName: String) -> Observable<Discoverable> {
        subscribe(deviceName: deviceName)
        bluetoothService.startScanning()
        return scanSubject.asObservable()
    }
}
