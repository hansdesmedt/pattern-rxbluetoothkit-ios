//
//  Commands.swift
//  RxBluetoothKitSampleApp
//
//  Created by Hans De Smedt on 05/12/2018.
//  Copyright © 2018 In The Pocket. All rights reserved.
//

import RxSwift
import RxBluetoothKit

struct ChunkConfigurable {
    var chunkSize: Int
    var chunkInterval: Int
    var chunkDelay: Int
}

final class Commands {
    
    var disposeBag = DisposeBag()
    let loggingSubject = PublishSubject<String>()
    let progressSubject = PublishSubject<Int>()
    let milliSecondsSubject = PublishSubject<Int>()
    
    private let bluetoothService: RxBluetoothKitService
    private var writeCharacteristic: Characteristic
    private var readCharacteristic: Characteristic
    private var chunkConfigurable: ChunkConfigurable?
    
    private var task: SendDataTask? = nil
    private var nextPayload: Data? = nil
    
    private var startTime = Date().timeIntervalSince1970
    private var stopTime = Date().timeIntervalSince1970
    
    var subject = PublishSubject<String>()
    
    let generalInfoSubject = PublishSubject<String>()
    let setOtaDataSubject = PublishSubject<String>()
    let getOtaDataSubject = PublishSubject<String>()
    let otaStartSubject = PublishSubject<String>()
    let otaWriteTestSubject = PublishSubject<String>()
    let sendTestFileSubject = PublishSubject<String>()
    let otaCheckTestSubject = PublishSubject<String>()
    let otaWriteSubject = PublishSubject<String>()
    let sendFileSubject = PublishSubject<String>()
    let otaCheckSubject = PublishSubject<String>()
    let otaCopySubject = PublishSubject<String>()
    
    init(bluetoothService: RxBluetoothKitService, discoverable: Discoverable, chunkConfigurable: ChunkConfigurable? = nil) {
        self.bluetoothService = bluetoothService
        self.writeCharacteristic = discoverable.writeCharacteristic
        self.readCharacteristic = discoverable.readCharacteristic
        self.chunkConfigurable = chunkConfigurable
        subscribe()
    }
    
    private func subscribe() {
        bluetoothService.updatedValueAndNotificationOutput.subscribe(onNext: { [weak self] (result) in
            switch result {
            case .success(let value):
                guard let data = value.value else { return }
                self?.loggingSubject.onNext("Data recieved from remote peer: \(data.hexEncodedString())")
                self?.subject.onNext(data.hexEncodedString())
            case .error(let error):
                self?.subject.onError(error)
                self?.loggingSubject.onNext("Read value error \(error)")
            }
        }).disposed(by: disposeBag)
        bluetoothService.observeValueUpdateAndSetNotification(for: readCharacteristic)
        
        self.bluetoothService.writeValueOutput.subscribe(onNext: { [unowned self] (result) in
            switch result {
            case .success:
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.writeNextPayloadSucceeded()
                }
            case .error:
                self.writeNextPayloadFailed()
            }
        }).disposed(by: self.disposeBag)
    }
    
    private func writeNextPayloadSucceeded() {
        guard let nextPayload = self.nextPayload, let task = self.task else { return }
        stopTime = Date().timeIntervalSince1970
        let millis = Int((stopTime - startTime) * 1000)
        
        progressSubject.onNext(task.percentage)
        milliSecondsSubject.onNext(millis)
        
        let totalSent = task.offset + nextPayload.count
        print("Writing value success in \(millis)ms, total \(totalSent)bytes data sent")

        task.offset += nextPayload.count
        if !task.sentAllData, let nextPayload = task.nextPayload {
            self.nextPayload = nextPayload
            startTime = Date().timeIntervalSince1970
            if let chunkConfigurable = chunkConfigurable, task.offset % (chunkConfigurable.chunkInterval * chunkConfigurable.chunkSize) == 0 {
                let delay = Double(chunkConfigurable.chunkDelay) / 1000
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                    print("delay: \(delay)")
                    print("data: \(nextPayload.hexEncodedString())")
                    self.bluetoothService.writeValueTo(characteristic: self.writeCharacteristic, data: nextPayload)
                }
            } else {
                print("data: \(nextPayload.hexEncodedString())")
                bluetoothService.writeValueTo(characteristic: writeCharacteristic, data: nextPayload)
            }
        } else if task.sentAllData {
            loggingSubject.onNext("All data is sent")
            subject.onNext("All data is sent")
            progressSubject.onNext(task.percentage)
            self.task = nil
            self.nextPayload = nil
        }
    }
    
    private func writeNextPayloadFailed() {
        loggingSubject.onNext("Writing value failed")
    }
    
    private func makeObservable(command: String, subject: PublishSubject<String>) -> Observable<String> {
        self.subject = subject
        if let data = command.hexadecimal {
            bluetoothService.writeValueTo(characteristic: writeCharacteristic, data: data)
        }
        return subject.asObservable().take(1)
    }
    
    private func makeFileObservable(filePath: String, subject: PublishSubject<String>) -> Observable<String> {
        self.subject = subject
        do {
            let url = URL(fileURLWithPath: Bundle.main.bundlePath.appending(filePath))
            let dataToSend = try Data(contentsOf: url)
            let dataToSendLength = dataToSend.count
            self.loggingSubject.onNext("Will send test file of \(dataToSendLength) bytes")
            task = SendDataTask(data: dataToSend)
            if let chunkConfigurable = chunkConfigurable {
                task = SendDataTask(data: dataToSend, size: chunkConfigurable.chunkSize)
            }            
            if let nextPayload = self.task?.nextPayload {
                self.nextPayload = nextPayload
                self.startTime = Date().timeIntervalSince1970
                self.bluetoothService.writeValueTo(characteristic: writeCharacteristic, data: nextPayload)
            }
        } catch let error {
            subject.onError(error)
        }
        return subject.asObservable().take(1)
    }
    
    func generalInfo() -> Observable<String> {
        return makeObservable(command: "00060000000000", subject: generalInfoSubject)
    }
    
    func setOta() -> Observable<String> {
        return makeObservable(command: "000c0043f05302001454020014", subject: setOtaDataSubject)
    }
    
    func getOta() -> Observable<String> {
        return makeObservable(command: "00060003f00000", subject: getOtaDataSubject)
    }
    
    func otaStart() -> Observable<String> {
        return makeObservable(command: "00130043f1010d80000000000000000000000000", subject: otaStartSubject)
    }
    
    func otaWriteTest() -> Observable<String> {
        return makeObservable(command: "00130043f1010d01000000000001010100000000", subject: otaWriteTestSubject)
    }
    
    func sendTestFile() -> Observable<String> {
        return makeFileObservable(filePath: "/test.bin", subject: sendTestFileSubject)
    }
    
    func otaCheckTest() -> Observable<String> {
        return makeObservable(command: "00130043f1010d0200010101000000000002c603", subject: otaCheckTestSubject)
    }
    
    func otaWrite() -> Observable<String> {
        return makeObservable(command: "00130043f1010d010000000000027fff00000000", subject: otaWriteSubject)
    }
    
    func sendFile() -> Observable<String> {
        return makeFileObservable(filePath: "/test_jurgen.bin", subject: sendTestFileSubject)
    }
    
    func otaCheck() -> Observable<String> {
        return makeObservable(command: "00130043f1010d0200027fff0000000000785103", subject: otaCheckSubject)
    }
    
    func otaCopy() -> Observable<String> {
        return makeObservable(command: "00130043f1010d0400027fff00037fff00000000", subject: otaCopySubject)
    }
}
