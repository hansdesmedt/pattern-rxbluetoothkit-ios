import UIKit

struct Constant {
    struct Strings {
        static let defaultDispatchQueueLabel = "com.polidea.rxbluetoothkit.timer"
    }
}
