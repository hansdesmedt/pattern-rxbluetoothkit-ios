//
//  String+Hexadecimal.swift
//  RxBluetoothKitSampleApp
//
//  Created by Hans De Smedt on 05/12/2018.
//  Copyright © 2018 In The Pocket. All rights reserved.
//

import Foundation

extension String {
    var hexadecimal: Data? {
        var data = Data(capacity: characters.count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }
        
        guard data.count > 0 else { return nil }
        
        return data
    }
}
