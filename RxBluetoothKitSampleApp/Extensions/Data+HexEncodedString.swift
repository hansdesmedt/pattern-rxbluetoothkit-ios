//
//  Data+HexEncodedString.swift
//  RxBluetoothKitSampleApp
//
//  Created by Hans De Smedt on 05/12/2018.
//  Copyright © 2018 In The Pocket. All rights reserved.
//

import Foundation

extension Data {
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
