//
//  ViewController.swift
//  RxBluetoothKitSampleApp
//
//  Created by Hans De Smedt on 03/12/2018.
//  Copyright © 2018 In The Pocket. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxBluetoothKit

enum SampleErrors: Error {
    case commandServiceMissing
}

// FAILED ON
// 55980bytes
// 157900bytes rc went red
// 300bytes in testfile app verstuurd niets meer?
// 58260bytes long time to send package, rc disconnects
// 52220bytes long time to send package, rc disconnects
// 182040bytes

class ViewController: UIViewController {
    
    @IBOutlet weak var deviceName: UITextField!
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var millisecondsLabel: UILabel!
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var disconnectButton: UIButton!
    
    @IBOutlet weak var sizeTextField: UITextField!
    @IBOutlet weak var intervalTextField: UITextField!
    @IBOutlet weak var delayTextField: UITextField!
    @IBOutlet weak var throttlingSwitch: UISwitch!
    
    private var bluetoothService = RxBluetoothKitService()
    private var discoverService: Discover? = nil
    private var commandsService: Commands? = nil
    private var peripheral: Peripheral? = nil
    private var disposeBag = DisposeBag()
    private var blueColor = UIColor.white.withAlphaComponent(0)
    private var transparantColor = UIColor.white.withAlphaComponent(0)
    private var totalStartTime = Date().timeIntervalSince1970
    private var totalStopTime = Date().timeIntervalSince1970
    
    override func viewDidLoad() {
        super.viewDidLoad()
        disconnectButton.layer.cornerRadius = 10
        connectButton.layer.cornerRadius = 10
        blueColor = connectButton.backgroundColor ?? UIColor.blue
        reset()
    }
    
    private func reset() {
        deviceName.isEnabled = true
        enable(button: connectButton)
        resetProgress(hide: true, animated: false)
        disable(button: disconnectButton)
        
        totalStartTime = Date().timeIntervalSince1970
        totalStopTime = Date().timeIntervalSince1970
        
        discoverService = Discover(bluetoothService: bluetoothService)
        commandsService = nil
        bluetoothService = RxBluetoothKitService()
        disposeBag = DisposeBag()
        
        discoverService?.loggingSubject
            .observeOn(MainScheduler.instance)
            .do(onNext: { [unowned self] (message) -> Void in
                self.logStatus(status: message)
            })
            .subscribe(onNext: { (_) in })
            .disposed(by: disposeBag)
    }
    
    private func resetProgress(hide: Bool, animated: Bool = true) {
        if !hide {
            percentageLabel.text = "0%"
            millisecondsLabel.text = "0ms"
        }
        hideProgress(hidden: hide, animated: animated)
    }
    
    private func disable(button: UIButton) {
        button.isEnabled = false
        button.backgroundColor = transparantColor
        button.setTitleColor(UIColor.lightGray, for: UIControl.State.normal)
    }
    
    private func enable(button: UIButton) {
        button.isEnabled = true
        button.backgroundColor = blueColor
        button.setTitleColor(UIColor.white, for: UIControl.State.normal)
    }
    
    private func hideProgress(hidden: Bool, animated: Bool = false) {
        if animated {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                UIView.transition(with: self.view, duration: 0.25, options: .transitionCrossDissolve, animations: {
                    self.percentageLabel.isHidden = hidden
                    self.millisecondsLabel.isHidden = hidden
                })
            }
        } else {
            percentageLabel.isHidden = hidden
            millisecondsLabel.isHidden = hidden
        }
    }
    
    private func didConnect() {
        enable(button: disconnectButton)
        totalStartTime = Date().timeIntervalSince1970
    }
    
    private func subscribe() {
        self.bluetoothService.disconnectionReasonOutput.subscribe(onNext: { [unowned self] (result) in
            switch result {
            case .success(let value):
                self.reset()
                self.logStatus(status: "Disconnected \(value.0.identifier)")
            case .error(let error):
                self.logStatus(status: "Disconnect error \(error)")
            }
            }, onError: { (error) in
                self.logStatus(status: "Disconnect error \(error)")
        }, onCompleted: {
            self.logStatus(status: "Disconnect completed")
        }, onDisposed: nil).disposed(by: disposeBag)
    }
    
    @IBAction func handleFirmwareUpdate(_ sender: UIButton) {
        guard let deviceNameText = deviceName.text else { return }
        
        sizeTextField.resignFirstResponder()
        intervalTextField.resignFirstResponder()
        delayTextField.resignFirstResponder()
        
        subscribe()
        deviceName.isEnabled = false
        disable(button: connectButton)
        view.backgroundColor = UIColor.white
        discoverService?.scan(deviceName: deviceNameText)
            .flatMap({ [unowned self] (discoverable) -> Observable<Commands> in return self.processDiscover(discoverable: discoverable) })
            .flatMap({ [unowned self] (_) -> Observable<String> in return self.processCommands() })
            .subscribe(onNext: { (test) in
                print("UPDATE SUCCESS")
            }, onError: { (error) in
                print("UPDATE FAILED \(error)")
                self.view.backgroundColor = UIColor.red
                self.disconnect(self.disconnectButton)
                self.reset()
            }, onCompleted: nil, onDisposed: nil)
            .disposed(by: disposeBag)
    }
    
    @IBAction func disconnect(_ sender: UIButton) {
        guard let peripheral = self.peripheral else { return }
        bluetoothService.disconnect(peripheral)
    }
    
    private func processDiscover(discoverable: Discoverable) -> Observable<Commands> {
        print("readCharacteristic \(discoverable.readCharacteristic.uuid) writeCharacteristic \(discoverable.writeCharacteristic.uuid)")
        
        var chunkSize:Int = 0
        if let size = sizeTextField.text {
            chunkSize = Int(size) ?? 0
        }
        var chunkInterval:Int = 0
        if let interval = intervalTextField.text {
            chunkInterval = Int(interval) ?? 0
        }
        var chunkDelay:Int = 0
        if let delay = delayTextField.text {
            chunkDelay = Int(delay) ?? 0
        }
        
        var chunkConfigurable: ChunkConfigurable? = nil
        if throttlingSwitch.isOn {
            chunkConfigurable = ChunkConfigurable(chunkSize: chunkSize, chunkInterval: chunkInterval, chunkDelay: chunkDelay)
        }
        
        let service = Commands(bluetoothService: self.bluetoothService, discoverable: discoverable, chunkConfigurable: chunkConfigurable)
        
        service.loggingSubject
            .observeOn(MainScheduler.instance)
            .do(onNext: { [unowned self] (message) -> Void in
                self.logStatus(status: message)
            })
            .subscribe(onNext: { (_) in })
            .disposed(by: disposeBag)
        
        service.progressSubject
            .observeOn(MainScheduler.instance)
            .map({ [unowned self] (progress) -> String in
                if progress > 0 {
                    self.hideProgress(hidden: false, animated: true)
                } else if progress == 100 {
                    self.hideProgress(hidden: true, animated: true)
                }
                return "\(progress)%" })
            .bind(to: percentageLabel.rx.text)
            .disposed(by: disposeBag)
        
        service.milliSecondsSubject
            .observeOn(MainScheduler.instance)
            .map({ (millis) -> String in return "\(millis)ms" })
            .bind(to: millisecondsLabel.rx.text)
            .disposed(by: disposeBag)
        
        peripheral = discoverable.peripheral
        commandsService = service
        didConnect()
        return service.generalInfo().map({ (_) -> Commands in return service })
    }
    
    private func processCommands() -> Observable<String> {
        guard let service = self.commandsService else { return Observable.empty() }
        return Observable.just(()).delay(1, scheduler: MainScheduler.instance)
            .flatMap({ (_) -> Observable<String> in return service.setOta() })
            .delay(1, scheduler: MainScheduler.instance)
            .flatMap({ (_) -> Observable<String> in return service.getOta() })
            .delay(1, scheduler: MainScheduler.instance)
            .flatMap({ (_) -> Observable<String> in return service.otaStart() })
            .delay(5, scheduler: MainScheduler.instance)
            .flatMap({ (_) -> Observable<String> in return service.otaWriteTest() })
            .delay(1, scheduler: MainScheduler.instance)
            .flatMap({ (_) -> Observable<String> in return service.sendTestFile() })
            .delay(6, scheduler: MainScheduler.instance)
            .flatMap({ (_) -> Observable<String> in return service.otaCheckTest() })
            .delay(1, scheduler: MainScheduler.instance)
            .flatMap({ (_) -> Observable<String> in return service.otaWrite() })
            .delay(1, scheduler: MainScheduler.instance)
            .flatMap({ (_) -> Observable<String> in return service.sendFile() })
            .delay(9, scheduler: MainScheduler.instance)
            .flatMap({ (_) -> Observable<String> in return service.otaCheck() })
//            .delay(1, scheduler: MainScheduler.instance)
//            .flatMap({ (_) -> Observable<String> in return service.otaCopy() })
    }
    
    private func logStatus(status: String) {
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            print(status)
            self.stepLabel.text = status
        }
    }
}
