//
//  SendDataTask.swift
//  RxBluetoothKitSampleApp
//
//  Created by Hans De Smedt on 05/12/2018.
//  Copyright © 2018 In The Pocket. All rights reserved.
//

import Foundation

final class SendDataTask {
    let data: Data
    let size: Int
    var offset = 0
    
    var maximumPayloadLength: Int {
        return size
    }
    
    var lengthOfRemainingData: Int {
        return data.count - offset
    }
    
    var percentage: Int {
        return Int(Double(offset) / Double(data.count) * 100)
    }
    
    var sentAllData: Bool {
        return lengthOfRemainingData == 0
    }
    
    var rangeForNextPayload: Range<Int>? {
        let lenghtOfNextPayload = maximumPayloadLength <= lengthOfRemainingData ? maximumPayloadLength : lengthOfRemainingData
        let payLoadRange = NSRange(location: offset, length: lenghtOfNextPayload)
        return Range(payLoadRange)
    }
    
    var nextPayload: Data? {
        if let range = rangeForNextPayload {
            return data.subdata(in: range)
        } else {
            return nil
        }
    }
    
    init(data: Data, size: Int = 20) {
        self.data = data
        self.size = size
    }
}
